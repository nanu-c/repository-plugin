class RepositoryPluginContentController < CmsController

  def uses_design_blocks?
    false
  end

  def config_content(content, parent, title)
    counter = 1
    title_orig = title
    while parent.children.find_by_slug title.to_slug
      counter += 1
      title = title_orig + " (#{counter})"
    end
    content.name = title
    content.show_to_followers = parent.show_to_followers
    content.profile = parent.profile
    content.parent = parent
    content.author = user
    content.editor = user.editor
    content.last_changed_by = user
    content.created_by = user
    content
  end

  def new
    @parent = check_parent params[:parent_id]
    unless @parent && @parent.is_a?(RepositoryPlugin::Repository)
      throw _('The parent is not a repository.')
    end

    if params[:article].blank?
      @article = Article.new
    else
      type = params[:type] || 'TextArticle'
      klass = type.constantize
      if type == 'UploadedFile'
        args = {profile: profile}.merge(
          uploaded_data: params[:uploaded_file],
          abstract: params[:article][:body]
        )
      else
        args = {profile: profile}.merge(params[:article]||{})
        args = args.merge(params[:event]) if type == 'Event'
      end
      title = params[:article][:name].to_s
      @article = config_content klass.new(args), @parent, title
      unless params[:article][:tag_list].blank?
        @article.tags = Tag.find_or_create_all_with_like_by_name params[:article][:tag_list].split(',')
      end
      @article.license_id = params[:article][:license_id]
      unless params[:metadata].blank?
        params[:metadata].each do |k,v|
          @article.metadata[k] = v
        end
      end
    end

    if request.get?
      @target = '/%s/%s' % [profile.identifier, @parent.full_name]
      return render file: 'repository_plugin_new_with_metadata', layout: get_layout
    elsif request.post?
      @article.save
      if @article.errors.any?
        return render file: 'repository_plugin_new_with_metadata', layout: get_layout
      end
      url = @article.url.merge((type == 'UploadedFile')? {view: true} : {})
      return redirect_to params[:back_to].blank? ? url : params[:back_to]
    else
      throw _('Cant accept this request.')
    end
  end

end
