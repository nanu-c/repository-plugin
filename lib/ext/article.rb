require_dependency 'article'

class Article

   # ActiveRecord::HasManyThroughAssociationNotFoundError:
   # Could not find the association :repository_plugin_metadatas in model Article
#  has_one :repo_metadata,
#    through: :repository_plugin_metadatas,
#    foreign_key: :article_id,
#    dependent: :destroy


  def repo_metadata
    RepositoryPlugin::Metadata.find_by_article_id id
  end

end
