class RepositoryPlugin::Repository < Folder

  def self.short_description
    _('Repository')
  end

  def self.description
    _('A folder to store medias with related meta-data.')
  end

  def self.icon_name(article = nil)
    'repository'
  end

  def accept_uploads?
    false
  end

  def allow_create?(user)
    false
  end

  def to_html(options = {})
    # Add a fu**ing view=true to uploaded_file links.
    folder_render = super options
    lambda do |context|
      instance_eval(&folder_render) +
      javascript_tag('
        $(".article-body-repository-plugin_repository .item-description a")
        .each(function(n,a){ a.href += "?view=true" })
      ')
    end
  end

end
