class CreateRepositoryPluginMetadatas < ActiveRecord::Migration

  def self.up
    create_table :repository_plugin_metadatas do |t|
      t.references  :article
      t.string      :groups
      t.string      :media_type
      t.string      :author
      t.date        :date
      t.string      :source
      t.string      :place_of_production
      t.string      :places_mentioned
      t.string      :names_mentioned
      t.string      :comment
    end
  end

  def self.down
    drop_table :repository_plugin_metadatas
  end

end
