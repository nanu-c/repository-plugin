class RemoveRepositoryPluginMetadatas < ActiveRecord::Migration

  def self.up
    drop_table :repository_plugin_metadatas
  end

end
